# Fabric.js 学习Demo

## 仓库简介

学习 `fabric.js` ，看文档不如看 `Demo` 。

这个仓库存放了我学习 `fabric.js` 的历程。

其中包括：
- 常用 `api` 的使用方法；
- 功能实战 `demo`；

<br><br>

## 学习资料

<br>

> 循序渐进的学起来 **[《Fabric.js 中文教程》](https://k21vin.gitee.io/fabric-js-doc)** 👍👍👍

<br>


> 好玩好用的 Demo 讲解 **[《Fabric.js 专栏》](https://juejin.cn/column/7050370347324932132)** 🦄🦄🦄

<br>

| 图文教程 | 原生代码 | 在vue3中的用法 |
|---|---|---|
| [创建画布](https://juejin.cn/post/7026941253845516324#heading-13) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/newCanvas.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Basic/pages/Stated/Stated.vue) |
| [设置画布背景色](https://juejin.cn/post/7026941253845516324#heading-20) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/backgroundColor.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Basic/pages/Stated/Stated.vue) |
| [动态设置背景宽高](https://juejin.cn/post/7053049468601499684) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/UploadImg/index.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Basic/pages/SetCanvasWH/SetCanvasWH.vue) |
| [将本地图像上传到画布背景](https://juejin.cn/post/7055201274693681160) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/setWH.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Demo/pages/UploadImg/UploadImg.vue) |
| [自定义右键菜单](https://juejin.cn/post/7051373700209180679) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/ContextMenu/index.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Demo/pages/ContextMenu/ContextMenu.vue) |
| [更换图片(处理存在缓存的情况)](https://juejin.cn/post/7052719026874613773) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/changeImage/index.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Demo/pages/ChangeImage/ChangeImage.vue) |
| [删除元素（带过渡动画）](https://juejin.cn/post/7056599707094614024) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/remove.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Basic/pages/RemoveObj/RemoveObj.vue) |
| [背景不受视口变换影响](https://juejin.cn/post/7105789686395699230) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/backgroundVpt.html) |  |
| [居中元素](https://juejin.cn/post/7105418456484282404) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/centerObject.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Basic/pages/CenterObject/CenterObject.vue) |
| 操作控件在裁剪区外显示 | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/controlsAboveOverlay.html) |  |
| [精简toObject](https://juejin.cn/post/7106159817361719304) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/includeDefaultValues.html) |  |
| [元素选中时保持原来层级](https://juejin.cn/post/7106530593986314253) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/preserveObjectStacking.html) |  |
| 元素选中时保持原来层级（按着alt可继续选中） | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/altSelectionKey.html) |  |
| [摆正元素](https://juejin.cn/post/7057392358391808008) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/straightenObject.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Basic/pages/StraightenObject/StraightenObject.vue) |
| 平移画布 | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/absolutePan.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Basic/pages/AbsolutePan/AbsolutePan.vue) |
| [缩放画布](https://juejin.cn/post/7105046849362329608) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/%E6%BB%9A%E8%BD%AE%E7%BC%A9%E6%94%BE%E7%94%BB%E5%B8%83.html) |  |
| [控制图层层级](https://juejin.cn/post/7111191499932434439) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Canvas/moveTo.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Basic/pages/MoveTo/MoveTo.vue) |
| [自由绘制矩形](https://juejin.cn/post/7058093223566114847) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/FreeDrawing/createRect.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Demo/pages/CreateRect/CreateRect.vue) |
| [自由绘制圆形](https://juejin.cn/post/7061277449652273165) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/FreeDrawing/createCircle.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Demo/pages/CreateCircle/CreateCircle.vue) |
| [自由绘制椭圆形](https://juejin.cn/post/7101906776202838024) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/FreeDrawing/createEllipse.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Demo/pages/CreateEllipse/CreateEllipse.vue) |
| 自由绘制三角形 | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/FreeDrawing/createTriangle.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Demo/pages/CreateTriangle/CreateTriangle.vue) |
| 自由绘制线段 | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/FreeDrawing/createLine.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Demo/pages/CreateLine/CreateLine.vue) |
| 自由绘制折线 | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/FreeDrawing/createPolyline.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Demo/pages/CreatePolyline/CreatePolyline.vue) |
| 自由绘制折线 | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/FreeDrawing/createPolygon.html) | [代码链接 Vue3版](https://gitee.com/k21vin/front-end-data-visualization/blob/master/src/views/FabricJS/Demo/pages/CreatePolygon/CreatePolygon.vue) |
| [基础笔刷](https://juejin.cn/post/7103569758473175070) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Brush/BaseBrush.html) | |
| [基础笔刷](https://juejin.cn/post/7103569758473175070) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Brush/BaseBrush.html) | |
| [圆形笔刷](https://juejin.cn/post/7104039103779307527) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/Brush/CircleBrush/CircleBrush.html) | |
| [手动加粗文本](https://juejin.cn/post/7101196583928070181) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/demos/ITextSetFontWeight/index.html) | |
| [手动设置文本斜体](https://juejin.cn/post/7111733967488811022) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/IText/styles05italic.html) | |
| [手动设置文本 上划线、中划线（删除线）、下划线](https://juejin.cn/post/7108489281764589604) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/IText/styles04decoration.html) | |
| [上标和下标](https://juejin.cn/post/7104799922230132743) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/IText/styles02superscript&subscript.html) | |
| [IText 激活输入框](https://juejin.cn/post/7107000176283222047) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/IText/enterEditing.html) | |
| [IText 动态设置字号大小](https://juejin.cn/post/7107767051162238990) | [代码链接](https://gitee.com/k21vin/fabricjs-demo/blob/master/tutorial/IText/styles03fontSize.html) | |

<br>

本仓库所有demo都使用原生方式开发。

**如果你想看看在 `Vue 3` 中如何使用 `fabric.js` ，可以看 [前端可视化笔记](https://gitee.com/k21vin/front-end-data-visualization) 这个仓库。**

如果你愿意的话，可以**帮我的文章点个赞**，即使只是打开一下文章也对我很有帮助的~~~

<br>

<br>

# 关于我

雷猴，我是德育处主任，一个喜欢吃喝玩乐的地球生物。只要和工作无关的事情我都觉得很有趣。

<br>

我正在整理前端可视化相关库的笔记，不定期在 [掘金](https://juejin.cn/column/7050370347324932132)、[CSDN](https://blog.csdn.net/weixin_39415598)、[思否](https://segmentfault.com/) 更新。

如果你想白嫖可以直接查看我公开的笔记，也可以点进我的 [掘金主页 **帮我点个赞~**](https://juejin.cn/column/7050370347324932132) 

<br>

已经停更了很久的 **订阅号（德育处主任）** 也有计划重启了，我打算把每个我学过的库整理成独立的专栏，以循序渐进的方式和大家一起摄入知识~

![公众号：德育处主任](./OfficialAccounts.gif)

<br>

> 最后最后，如果你觉得我的文章对你有帮助，可以请我的猫吃个罐头~
> 
> ![公众号：德育处主任](./q.png)

